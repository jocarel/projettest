package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import object.MyObject;

public class SommeTestJUnit {

	private MyObject obj = new MyObject(5,2);
	
	@Test
	public void addtest() {
		double db = obj.add(2);
		assertTrue(db == 0);
	}

}